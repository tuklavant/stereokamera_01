import numpy as np
import cv2
import json
 
with open('camera_matrices.json') as data_file: data = json.load(data_file)
 
camera_matrix_l = np.array(data['camera_matrix_l'])
dist_coeffs_l = np.array(data['dist_coeffs_l'])
rectification_matrix_l = np.array(data['rectification_matrix_l'])
projection_matrix_l = np.array(data['projection_matrix_l'])
w = data['w']
h = data['h']
 
map1_l, map2_l = cv2.initUndistortRectifyMap(camera_matrix_l, dist_coeffs_l, rectification_matrix_l, projection_matrix_l, (w[0], h[0]), cv2.CV_16SC2)
img = cv2.imread("images/left06.jpg", 0)
i = cv2.remap(img, map1_l, map2_l, cv2.INTER_LINEAR)
cv2.imshow('image', i)
cv2.waitKey(0)
#cv2.imwrite('test.bmp', i) # alternativ
cv2.destroyAllWindows()